# OpenGL course

A fork of https://git.tfk-astrodome.net/Teo-CD/OpenGL-JIN in order to do my own project on Linux
My project is based on his work, and is some commits behind it.

The goal of the course is to introduce us to the OpenGL render pipeline and how to use the library.

The code here was, in part, inspired by the code which was given as part of the course but could not run elsewhere than Windows.
It is aimed to replace it and serve as a base for my own project as part of the course.

## The project

A small game on a map. I wrote the code that loads the heightmap from an image and applies a texture to it, the code which loads a 3D obj file with its texture, and the deplacement.
You play a raptor (or a raptor rider) who needs to eat rabbits to stay alive. You use arrows to move, and each time you eat a rabbit, another one spawns nearby.
Your vertical position is clamped to the terrain height.

![Game screenshot](resources/eat_or_die_icon.png)

## Building
The windows build supports only supports the `vcpkg` package manager. You can either import the project directly in Visual Studio or use another IDE.
In order to use another IDE with vcpkg, make sure of the following:
 - `VCPKG_ROOT` is an environment variable and correctly points at the root of your vcpkg install.
 
### Dependencies

The following libraries must be installed and findable by CMake:
 - OpenGL
 - Freeglut 3
