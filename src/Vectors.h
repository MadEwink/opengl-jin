//
// Created by trotfunky on 30/09/2019.
//

#ifndef TESTS_OPENGL_VECTORS_H
#define TESTS_OPENGL_VECTORS_H

#include <fstream>
#include <cmath>

/// Group of coordinates with the input operator overloaded.
/// \tparam T Content of the vector
/// \tparam N Number of coordinates
template <typename T, unsigned int N>
struct CoordinatesVector
{
    T coordinates[N];

    /// Computes the euclidean norm of the vector.
    /// \return The magnitude.
    double magnitude() const
    {
        // TODO : Save magnitude if the vector has not been modified.
        double sum_of_squares = 0;
        for (unsigned int i = 0;i<N;i++)
        {
            sum_of_squares += coordinates[i]*coordinates[i];
        }
        return sqrt(sum_of_squares);
    }

    // Use SFINAE to allow normalization of floating point.
    // The template argument is used to force substitution and thus checking the enable_if.
    /// Normalizes the vector using its euclidean norm.
    /// \return Nothing, the vector is normalized in-place.
    template <typename U = T>
    typename std::enable_if<std::is_floating_point<U>::value,void>::type
    normalize()
    {
        double norm = magnitude();
        for (unsigned int i = 0;i<N;i++)
        {
            coordinates[i] /= norm;
        }
    }


    template <typename U = T>
    typename std::enable_if<std::is_arithmetic<U>::value,T>::type
    dot_product(const CoordinatesVector<T,N>& operand) const
    {
        T result = 0;
        for (unsigned int i = 0;i<N;i++)
        {
            result += coordinates[i]*operand.coordinates[i];
        }
        return result;
    }

    // Use SFINAE to declare the function if and only if the type is arithmetic
    template <typename Scalar>
    typename std::enable_if<std::is_arithmetic<Scalar>::value,CoordinatesVector<T,N>>::type
    operator*(const Scalar& scalar)
    {
        CoordinatesVector result;
        for (unsigned int i = 0;i<N;i++)
        {
            result.coordinates[i] = coordinates[i] * scalar;
        }
        return result;
    }

    CoordinatesVector operator+(const CoordinatesVector<T,N>& op)
    {
        CoordinatesVector result;
        for (unsigned int i = 0;i<N;i++)
        {
            result.coordinates[i] = coordinates[i] + op.coordinates[i];
        }
        return result;
    }

    CoordinatesVector operator-(const CoordinatesVector<T,N>& op)
    {
        CoordinatesVector result;
        for (unsigned int i = 0;i<N;i++)
        {
            result.coordinates[i] = coordinates[i] - op.coordinates[i];
        }
        return result;
    }

    CoordinatesVector& operator=(const CoordinatesVector<T,N>& original)
    {
        if (this != &original)
        {
            std::copy(std::begin(original.coordinates), std::end(original.coordinates), std::begin(coordinates));
        }
        return *this;
    }

    bool operator==(const CoordinatesVector<T,N>& op)
    {
        for (unsigned int i = 0;i<N;i++)
        {
            if (coordinates[i] != op.coordinates[i])
            {
                return false;
            }
        }
        return true;
    }

    bool operator!=(const CoordinatesVector<T,N>& op)
    {
        return !(*this == op);
    }

    friend std::fstream& operator>>(std::fstream& stream, CoordinatesVector<T,N>& vector)
    {
        for (unsigned int i = 0;i<N;i++)
        {
            stream >> vector.coordinates[i];
        }
        return stream;
    }
};

// Define some specializations to add x,y,z references to the coordinates.

template <typename T>
struct Vec2 : public CoordinatesVector<T,2>
{
    T& x = CoordinatesVector<T,2>::coordinates[0];
    T& y = CoordinatesVector<T,2>::coordinates[1];

    Vec2() = default;

    Vec2(T x_axis, T y_axis)
    {
        x = x_axis;
        y = y_axis;
    }

    Vec2(const Vec2<T>& original): x(CoordinatesVector<T,2>::coordinates[0]), y(CoordinatesVector<T,2>::coordinates[1])
    {
        std::copy(std::begin(original.coordinates), std::end(original.coordinates), std::begin(CoordinatesVector<T,2>::coordinates));
    }

    // Cast-copy constructor in order to use the operators of the mother class.
    Vec2(const CoordinatesVector<T,2>& origin)
    {
        if (this != &origin)
        {
            std::copy(std::begin(origin.coordinates),std::end(origin.coordinates),std::begin(CoordinatesVector<T,2>::coordinates));
        }
    }

    Vec2& operator=(const Vec2<T>& origin)
    {
        if (this != &origin)
        {
            std::copy(std::begin(origin.coordinates), std::end(origin.coordinates), std::begin(CoordinatesVector<T,2>::coordinates));
        }
        return *this;
    }
};

template <typename T>
struct Vec3 : CoordinatesVector<T,3>
{

    T& x = CoordinatesVector<T,3>::coordinates[0];
    T& y = CoordinatesVector<T,3>::coordinates[1];
    T& z = CoordinatesVector<T,3>::coordinates[2];

    Vec3() = default;

    Vec3(T x_axis, T y_axis, T z_axis)
    {
        x = x_axis;
        y = y_axis;
        z = z_axis;
    }

    Vec3(const Vec3<T>& original): x(CoordinatesVector<T,3>::coordinates[0]), y(CoordinatesVector<T,3>::coordinates[1]), z(CoordinatesVector<T,3>::coordinates[2])
    {
        std::copy(std::begin(original.coordinates), std::end(original.coordinates), std::begin(CoordinatesVector<T,3>::coordinates));
    }

    // Cast-copy constructor in order to use the operators of the mother class.
    Vec3(const CoordinatesVector<T,3>& original)
    {
        if (this != &original)
        {
            std::copy(std::begin(original.coordinates), std::end(original.coordinates), std::begin(CoordinatesVector<T,3>::coordinates));
        }
    }

    template <typename U = T>
    typename std::enable_if<std::is_arithmetic<U>::value,Vec3<T>>::type
    cross_product(const Vec3<T>& operand) const
    {
        Vec3<T> normal_vector{0,0,0};
        normal_vector.x = y*operand.z - z*operand.y;
        normal_vector.y = z*operand.x - x*operand.z;
        normal_vector.z = x*operand.y - y*operand.x;

        return normal_vector;
    }

    Vec3& operator=(const Vec3<T>& original)
    {
        if (this != &original)
        {
            std::copy(std::begin(original.coordinates), std::end(original.coordinates), std::begin(CoordinatesVector<T,3>::coordinates));
        }
        return *this;
    }
};


// Define aliases for common types
typedef Vec2<int> Vec2i;
typedef Vec2<float> Vec2f;

typedef Vec3<int> Vec3i;
typedef Vec3<float> Vec3f;
typedef Vec3<double> Vec3d;

#endif //TESTS_OPENGL_VECTORS_H
