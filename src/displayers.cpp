//
// Created by trotfunky on 24/09/2019.
//

#include "displayers.h"

void display_pyramid(float x, float y, float z, float c, float h)
{
    glBegin(GL_TRIANGLES);

    // First side
    glColor3f(1,0,0);

    // Summit
    glVertex3f(x,y+h,z);
    glVertex3f(x+c/2,y,z+c/2);
    glVertex3f(x-c/2,y,z+c/2);

    // Second side
    glColor3f(0,1,0);

    glVertex3f(x,y+h,z);
    glVertex3f(x+c/2,y,z+c/2);
    glVertex3f(x+c/2,y,z-c/2);

    // Third side
    glColor3f(0,0,1);

    glVertex3f(x,y+h,z);
    glVertex3f(x-c/2,y,z+c/2);
    glVertex3f(x-c/2,y,z-c/2);

    // Fourth side
    glColor3f(1,1,0);

    glVertex3f(x,y+h,z);
    glVertex3f(x-c/2,y,z-c/2);
    glVertex3f(x+c/2,y,z-c/2);

    glEnd();
    // Bottom
    glBegin(GL_QUADS);

    glColor3f(0,1,1);
    glVertex3f(x-c/2,y,z-c/2);
    glVertex3f(x-c/2,y,z+c/2);
    glVertex3f(x+c/2,y,z+c/2);
    glVertex3f(x+c/2,y,z-c/2);

    glEnd();
}

void display_rotating_pyramid(float x, float y, float z, float c, float h, float alpha)
{
    glPushMatrix();
    glTranslatef(x,0,z);
    glRotatef(alpha,0,1,0);
    glTranslatef(-x,0,-z);
    display_pyramid(x,y,z,c,h);
    glPopMatrix();
}

void display_tree(float x, float y, float z, float h, float w, const Texture& tree_texture)
{
    glEnable(GL_ALPHA_TEST);

    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    glBindTexture(GL_TEXTURE_2D,tree_texture.opengl_id[0]);

    glColor4f(1.0f, 1.0f, 1.0f,1.00);
    glBegin(GL_QUADS);

    // First rectangle
    glTexCoord2f(1,0);
    glVertex3f(x+w/2,y,z);

    glTexCoord2f(0,0);
    glVertex3f(x-w/2,y,z);

    glTexCoord2f(0,1);
    glVertex3f(x-w/2,y+h,z);

    glTexCoord2f(1,1);
    glVertex3f(x+w/2,y+h,z);

    // Second rectangle
    glTexCoord2f(1,0);
    glVertex3f(x,y,z+w/2);

    glTexCoord2f(0,0);
    glVertex3f(x,y,z-w/2);

    glTexCoord2f(0,1);
    glVertex3f(x,y+h,z-w/2);

    glTexCoord2f(1,1);
    glVertex3f(x,y+h,z+w/2);

    glEnd();

    glDisable(GL_ALPHA_TEST);
}
