//
// Created by trotfunky on 18/10/2019.
//

#include "Quaternion.h"


Quaternion::Quaternion(double axis_x, double axis_y, double axis_z, double w_value) : CoordinatesVector<double,4>()
{
    x = axis_x;
    y = axis_y;
    z = axis_z;
    w = w_value;
}

Quaternion::Quaternion(const Quaternion& original) : x(coordinates[0]), y(coordinates[1]), z(coordinates[2]), w(coordinates[3])
{
    std::copy(std::begin(original.coordinates),std::end(original.coordinates),std::begin(coordinates));
}

Quaternion::Quaternion(const CoordinatesVector<double, 4>& original)
{
    if (this != &original)
    {
        std::copy(std::begin(original.coordinates),std::end(original.coordinates),std::begin(coordinates));
    }
}

Quaternion::Quaternion(const CoordinatesVector<double, 3>& original)
{
    std::copy(std::begin(original.coordinates),std::end(original.coordinates),std::begin(coordinates));
    w = 0;
}

Quaternion::operator Vec3d() const
{
    Vec3d result;
    std::copy(std::begin(coordinates),std::begin(coordinates)+3,std::begin(result.coordinates));
    return result;
}

Quaternion Quaternion::inverse() const
{
    Quaternion inverse = *this;
    inverse.x *= -1;
    inverse.y *= -1;
    inverse.z *= -1;
    return inverse;
}

Quaternion Quaternion::operator*(const Quaternion& op) const
{
    Quaternion result;
    result.w = w*op.w - x*op.x - y*op.y - z*op.z;
    result.x = w*op.x + x*op.w + y*op.z - z*op.y;
    result.y = w*op.y - x*op.z + y*op.w + z*op.x;
    result.z = w*op.z + x*op.y - y*op.x + z*op.w;
    return result;
}

Vec3d operator*(const Quaternion& rot, const Vec3d& op)
{
    Quaternion originalPosition = op;
    return static_cast<Vec3d>(rot * originalPosition * rot.inverse());
}

Vec3d operator*(const Vec3d& op,const Quaternion& rot)
{
    Quaternion originalPosition = op;
    return static_cast<Vec3d>(rot.inverse() * originalPosition * rot);
}

Quaternion& Quaternion::operator=(const Quaternion& original)
{
    if (this != &original)
    {
        std::copy(std::begin(original.coordinates),std::end(original.coordinates),std::begin(coordinates));
    }
    return *this;
}
