//
// Created by trotfunky on 30/09/2019.
//

#ifndef TESTS_OPENGL_MODEL3D_H
#define TESTS_OPENGL_MODEL3D_H

#include <string>
#include <fstream>
#include <iostream>
#include "GL/glut.h"

#include "Vectors.h"
#include "Texture.h"

class Model3D {
public:
    Model3D();
    explicit Model3D(const std::string& file_name);
    ~Model3D();

    /// Loads an ASCII OFF file. Detects if there is texture data. Only triangles are supported.
    bool load_obj_file(const std::string& file_name);
    bool load_ascii_off_file(const std::string& file_name);
    bool assign_texture(Texture& new_texture);

    void set_scaling(float x_scale, float y_scale, float z_scale);
    void set_rotation(float angle, float x, float y, float z);

    void draw_model();

    bool is_textured;
private:
    uint32_t vertex_count;
    uint32_t face_count;

    /// Coordinates of the vertices
    Vec3f* vertices{};
    /// Indices of the vertices making up each face
    Vec3i* faces{};
    /// Normals of each vertex
    Vec3f* normals{};

    uint32_t texture_count;
    /// U,V coordinates inside the texture
    Vec2f* texture_coordinates{};
    /// Indices of the texture coordinates of each point on a face
    Vec3i* face_textures{};

    Texture* texture{};

    Vec3f scaling{};
    Vec3f rotation_axis{};
    float rotation_angle{};
};


#endif //TESTS_OPENGL_MODEL3D_H
