//
// Created by trotfunky on 24/09/2019.
//

#ifndef TESTS_OPENGL_TEXTURE_H
#define TESTS_OPENGL_TEXTURE_H

#include <string>
#include <fstream>
#include <iostream>
#include <GL/glut.h>

class Texture {
public:
    uint16_t width;
    uint16_t height;

    /// Number of bits describing the colors : 24 (RGB) or 32 (RGBA)
    uint8_t color_bits;

    uint8_t* image_data;
    uint32_t opengl_id[1];

    Texture();
    ~Texture();

    /// Load an RGB image from an RGB TGA file
    bool load_rgb_tga(const std::string& rgb_filename);
    /// Load an RGBA image from an rgb and an alpha (Greyscale) tga file
    bool load_rgba_tga(const std::string& rgb_filename,const std::string& mask_filename);

private:
    /// Load and RGB TGA image file to an array
    bool load_tga(const std::string& filename, uint8_t*& data_array);

    void invert_channels(uint8_t first_channel,uint8_t second_channel);
};


#endif //TESTS_OPENGL_TEXTURE_H
