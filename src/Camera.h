//
// Created by trotfunky on 19/10/2019.
//

#ifndef TESTS_OPENGL_CAMERA_H
#define TESTS_OPENGL_CAMERA_H

#include <cmath>
#include <GL/glut.h>

#include "Quaternion.h"
#include "Vectors.h"

class Camera {
public:
    Camera(const Vec3d& eye_pos,const Vec3d& look_direction,const Vec3d& up_vector);

    /// Translates the camera relative to its current position and look direction.
    /// \param translation Translation where x is pointing in the direction of vision.
    void translate(const Vec3d& translation);
    /// Rotates the gaze around its local x and z but around global y, relatively to the current orientation.
    /// This is to provide a coherent movement in regards to mouse movement.
    /// \param rotation Angles are radians to rotate about each axis.
    void rotate(const Vec3d& rotation);

    /// Calls the gluLookAt function for the camera.
    void look();

    void set_position(const Vec3d& eye_pos);
    Vec3d get_position();
    Vec3d get_gaze();
private:
    Vec3d eye;
    Vec3d gaze;
    Vec3d gaze_up;

    /// Quaternion from the world coordinates to the local coordinates
    Quaternion rotation_quaternion;

    /// Compute rotation quaternion from the global coordinates to the local coordinates.
    /// As per https://math.stackexchange.com/questions/2122668/calculate-orientation-quaternion-given-two-axes-of-a-coordinate-system
    void compute_base_change();
};


#endif //TESTS_OPENGL_CAMERA_H
